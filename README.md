[![pipeline](https://gitlab.com/apoirier42/ci-python/badges/master/pipeline.svg?style=flat-square)](https://gitlab.com/apoirier42/ci-python)
[![coverage](https://gitlab.com/apoirier42/ci-python/badges/master/coverage.svg?style=flat-square)](https://gitlab.com/apoirier42/ci-python)
[![pylint](https://apoirier42.gitlab.io/ci-python/pylint.svg)](https://gitlab.com/apoirier42/ci-python)

# CI Python

Explore how to use GitLab CI on Python code.

## Reports

### pylint

![pylint.txt](https://gitlab.com/apoirier42/ci-python/builds/artifacts/master/raw/public/pylint.txt?job=pages)

### Coverage

[https://apoirier42.gitlab.io/ci-python](https://apoirier42.gitlab.io/ci-python)
