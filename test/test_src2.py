import unittest
import src.src2


class TestMyFunctions(unittest.TestCase):

    def test_fct1_returns_true(self):
        self.assertTrue(src.src2.fct1())

    def test_fct2_returns_true(self):
        self.assertTrue(src.src2.fct2())
